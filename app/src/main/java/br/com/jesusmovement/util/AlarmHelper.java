package br.com.jesusmovement.util;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import br.com.jesusmovement.R;

/**
 * Created by Uzias on 9/25/15.
 */
public class AlarmHelper {

    public final static long DAY = 24 * 60 * 60 * 1000;
    public static final int NOTIFICATION_INACTIVITY_REQUEST_CODE = 100;
    private static final int ALARM_REQUEST_CODE_PENDING_INTENT = 101;

    public static void createAlarmInactivity(Context context) {
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context.getPackageName() +".INACTIVITY_WARNING");
        PendingIntent pIntent = PendingIntent.getBroadcast(context, ALARM_REQUEST_CODE_PENDING_INTENT, intent, 0);
        manager.cancel(pIntent);
        int days = Integer.parseInt(context.getString(R.string.alarm_manager_days));
        manager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + ( days * DAY), pIntent);
    }


    public static void closeNotifications(Context context) {
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(ALARM_REQUEST_CODE_PENDING_INTENT);
    }
}

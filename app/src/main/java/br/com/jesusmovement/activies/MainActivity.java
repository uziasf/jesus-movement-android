package br.com.jesusmovement.activies;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.appcompat.BuildConfig;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import java.io.File;
import java.io.InputStream;

import br.com.jesusmovement.R;
import br.com.jesusmovement.util.AlarmHelper;
import br.com.jesusmovement.util.ParseUtils;

public class MainActivity extends AppCompatActivity {

    public String url;
    WebView webView;
    View progressView;
    Context context;
    WebViewClient client = new WebViewClient(){
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressView.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressView.setVisibility(View.GONE);
                }
            }, 500);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if(url.contains(".ics")){
                DownloadManager mdDownloadManager = (DownloadManager) context
                        .getSystemService(Context.DOWNLOAD_SERVICE);
                DownloadManager.Request request = new DownloadManager.Request(
                        Uri.parse(url));
                File destinationFile = new File(Environment.getExternalStorageDirectory(), getFileName());
                request.setDescription("Downloading ...");
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationUri(Uri.fromFile(destinationFile));
                mdDownloadManager.enqueue(request);
                return true;
            }
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            progressView.setVisibility(View.GONE);
            try {
                view.stopLoading();
            } catch (Exception e) {
            }
            String str = "";
            try {
                InputStream is = getAssets().open("error.html");
                int size = is.available();

                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();

                str = new String(buffer);
                str = str.replace("$(text)", getString(R.string.somethingWrong));
                str = str.replace("$(button_text)", getString(R.string.buttonText));
                str = str.replace("$(url)", url);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
//            webView.loadUrl(String.valueOf(Html.fromHtml(str)));
            webView.loadDataWithBaseURL(null, str, "text/html", "UTF-8", null);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        url = getString(R.string.base_url);
        AlarmHelper.closeNotifications(this);
        AlarmHelper.createAlarmInactivity(this);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Drawable a = progressBar.getIndeterminateDrawable();
            if(a!=null) {
                a.setColorFilter(getResources().getColor(R.color.primary), PorterDuff.Mode.SRC_IN);
            }
        }

        context = getApplicationContext();

        webView = (WebView) findViewById(R.id.webview);

        progressView = findViewById(R.id.progressView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(client);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && BuildConfig.DEBUG) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        seTupView();
    }

    private void seTupView(){
        webView.loadUrl(url);
    }

    public String getFileName() {
        String filenameWithoutExtension = "";
        filenameWithoutExtension = String.valueOf(System.currentTimeMillis()
                + ".ics");
        return filenameWithoutExtension;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }



}
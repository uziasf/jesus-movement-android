package br.com.jesusmovement.receiver;

/**
 * Created by Uzias on 9/25/15.
 */
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import br.com.jesusmovement.R;
import br.com.jesusmovement.activies.MainActivity;
import br.com.jesusmovement.util.AlarmHelper;

public class NotificationAlarmReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        Intent launchApp = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, AlarmHelper.NOTIFICATION_INACTIVITY_REQUEST_CODE,
                launchApp, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(context.getString(R.string.notification_inactivity_text))
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(AlarmHelper.NOTIFICATION_INACTIVITY_REQUEST_CODE, mBuilder.build());
    }

}
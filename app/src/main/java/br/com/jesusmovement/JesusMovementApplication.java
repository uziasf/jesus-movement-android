package br.com.jesusmovement;

import android.app.Application;

import br.com.jesusmovement.util.ParseUtils;

/**
 * Created by Uzias on 8/22/15.
 */
public class JesusMovementApplication  extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        ParseUtils.registerParse(this);
    }
}

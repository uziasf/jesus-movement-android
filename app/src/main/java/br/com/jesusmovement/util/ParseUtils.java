package br.com.jesusmovement.util;

import android.content.Context;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import br.com.jesusmovement.R;

/**
 * Created by Uzias on 8/22/15.
 */
public class ParseUtils {
    private static String TAG = ParseUtils.class.getSimpleName();

    public static void registerParse(Context context) {
        // initializing parse library
        Parse.initialize(context,  context.getString(R.string.parse_application_id), context.getString(R.string.parse_client_key));
        ParseInstallation.getCurrentInstallation().saveInBackground();

        ParsePush.subscribeInBackground( context.getString(R.string.parse_channel), new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Log.e(TAG, "Successfully subscribed to Parse!");
            }
        });
    }

}
